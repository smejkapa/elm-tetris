module BlockType exposing (..)

import Point exposing (..)
import Matrix exposing (..)

type BlockType
    = O
    | T
    | I
    | J
    | L
    | S
    | Z

type alias Block =
    { type_: BlockType
    , position : Point
    , grid : Matrix Bool
    }


fromInt : Int -> BlockType
fromInt n =
    case n of
        0 ->
            O
        1 ->
            T
        2 ->
            I
        3 ->
            J
        4 ->
            L
        5 ->
            S
        _ ->
            Z

