module Msg exposing (..)

import Dir exposing (..)
import Time exposing (..)
import Keyboard
import BlockType exposing (..)

type Msg
    = Move Dir
    | Rotate
    | Tick Time
    | KeyMsg Keyboard.KeyCode
    | NewBlock BlockType