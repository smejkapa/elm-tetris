module Model exposing (..)

import BlockType exposing (..)
import Matrix exposing (..)
import Bounds exposing (..)

type alias Model =
    { piece : Block
    , field : Matrix Bool
    , score : Int
    }

getEmptyField : Int -> Int -> Matrix Bool
getEmptyField h w =
    matrix h w (\l -> False)

getEmptyModel : Block -> Bounds -> Model
getEmptyModel block bounds =
    Model block (getEmptyField bounds.right bounds.bot) 0
