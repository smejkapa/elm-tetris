# elm-tetris #
Made just to learn the language. As such has a bunch of weird things, hacks and bugs.

## How to run ##
1. Install [elm-lang 1.18](https://guide.elm-lang.org/install.html)
1. Clone the repository
1. In root folder of the repo run `elm-reactor`
1. Open `localhost:8000/Game.elm` in web browser