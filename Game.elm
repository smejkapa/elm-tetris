module Main exposing (..)

--import Html.Attributes exposing (..)

import Block exposing (..)
import Dir exposing (..)
import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (style)
import Model exposing (..)
import Msg exposing (..)
import Point exposing (..)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Time exposing (..)
import Matrix exposing (..)
import BlockType exposing (..)
import Bounds exposing (..)
import Keyboard exposing (..)
import BlockGenerator exposing (..)
import Random

--import Svg.Attributes exposing (..)


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


init : ( Model, Cmd Msg )
init =
    ( getEmptyModel (getNextBlock startPoint) bounds, Random.generate NewBlock blockType )

bounds : Bounds
bounds =
    Bounds 0 20 0 10


startPoint : Point
startPoint =
    { x = 2, y = -2 }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch [ Time.every (500 * millisecond) Tick
          , Keyboard.downs KeyMsg]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Move dir ->
            handleMove model dir

        Rotate ->
            handleRotate model

        NewBlock val ->
            ({model | piece = (makeBlock val startPoint)}, Cmd.none)

        Tick time ->
            let
                p =
                    model.piece
                ps =
                    p.position
                newPiece =
                    { p | position = { ps | y = ps.y + 1 } }
            in
            if isValidPos newPiece model.field False then
                ( { model | piece = newPiece }, Cmd.none )
            else
                let
                    (newField, linesCleared) =
                        manageField model.piece model.field
                    newScore =
                        model.score + scoreForLines linesCleared
                    newModel =
                        { model | piece = (getNextBlock startPoint), field = newField, score = newScore }
                in
                    if isValidPos model.piece model.field True &&
                     isValidPos newModel.piece newModel.field False then
                        ( newModel , Random.generate NewBlock blockType )
                    else
                        ( getEmptyModel (getNextBlock startPoint) bounds, Random.generate NewBlock blockType )

        KeyMsg code ->
            case code of
                38 -> -- up
                    handleRotate model
                37 -> -- left
                    handleMove model Left
                39 -> -- right
                    handleMove model Right
                40 ->
                    handleMove model Down
                _ -> -- down is 40
                    (model, Cmd.none)


handleRotate : Model -> (Model, Cmd msg)
handleRotate model =
    let
        newPiece =
            Block.rotate model.piece
    in
        if isValidPos newPiece model.field False then
            ( {model | piece = newPiece }, Cmd.none )
        else
            (model, Cmd.none)

handleMove : Model -> Dir -> ( Model, Cmd Msg )
handleMove model dir =
    let
        p =
            model.piece
        ps =
            p.position
    in
    case dir of
        Left ->
            let
                newPiece =
                    { p | position = { ps | x = ps.x - 1  } }
            in
                if isValidPos newPiece model.field False then
                    ( { model | piece = newPiece }, Cmd.none )
                else
                    ( model, Cmd.none )

        Right ->
            let
                newPiece =
                    { p | position = { ps | x = ps.x + 1  } }
            in
                if isValidPos newPiece model.field False then
                    ( { model | piece = newPiece }, Cmd.none )
                else
                    ( model, Cmd.none )
        Down ->
            let
                newPiece =
                    { p | position = { ps | y = ps.y + 1 } }
            in
                if isValidPos newPiece model.field False then
                    ( { model | piece = newPiece }, Cmd.none )
                else
                    ( model, Cmd.none )


manageField : Block -> Matrix Bool -> (Matrix Bool, Int)
manageField piece field =
    let
        added =
            mapWithLocation (
                \l v ->
                    let
                        local = loc (row l - piece.position.x) (Matrix.col l - piece.position.y)
                    in
                        Maybe.withDefault False (get local piece.grid) ||
                        v
                ) field
        removed = removeFull added
        diff = (colCount field) - (colCount removed)
        mat = toList removed
        result = fromList <| addEmpty diff mat
    in
        (result, diff)


addEmpty : Int -> List (List Bool) -> List (List Bool)
addEmpty i mat =
    let
        empty = List.repeat i False
    in
        List.map (\l -> empty ++ l) mat


removeFull : Matrix Bool -> Matrix Bool
removeFull field =
    let
        trans = toList <| transpose field
        fil = List.filter isNotFull trans

    in
        transpose <| fromList fil


isNotFull : List Bool -> Bool
isNotFull l =
    case l of
        [] ->
            False
        (True :: xs) ->
            isNotFull xs
        (False :: xs) ->
            True


isValidPos : Block -> Matrix Bool -> Bool -> Bool
isValidPos block field checkTop =
    let
        flat = flatten <| mapWithLocation (\l v -> (l, v)) block.grid
        filtered = List.filter (\(l, v) -> v) flat
        locs = List.map (\(l, v) -> Point (row l + block.position.x) (Matrix.col l + block.position.y)) filtered
        outOfBounds = List.filter (
            \p ->
                p.x < bounds.left ||
                p.x > bounds.right - 1 ||
                p.y > bounds.bot - 1 ||
                (checkTop && p.y < bounds.top)
            )
            locs
        colliding = List.filter (
            \p ->
                get (loc p.x p.y) field == Just True
        ) locs
    in
        List.isEmpty ( outOfBounds ++ colliding )


scoreForLines : Int -> Int
scoreForLines linesCleared =
    linesCleared ^ 2


-- Graphics

view : Model -> Html Msg
view model =
    div [width "800px"]
        [ div [Html.Attributes.style [("float", "left")]] [ graphics model
                 , debugInfo model
                 ]
        , div [] [ scoreView model.score
                 ]
        ]


graphics : Model -> Html Msg
graphics model =
    div []
        [ svg [ width "500px", height "600px" ]
            ([ rect [ width <| toString <| tileDim * bounds.right, height <| toString <| tileDim * bounds.bot, fill "#7B7B7B" ] []
            ] ++ drawBlock model.piece
            ++ drawField model.field)
        ]


drawField : Matrix Bool -> List (Html Msg)
drawField field =
    drawMatrix 0 field []


drawMatrix : Int -> Matrix Bool -> List(Html Msg) -> List (Html Msg)
drawMatrix i field acc =
    let
        c =
            i % colCount field

        r =
            i // colCount field
    in
        if r >= rowCount field then
            acc
        else if get (loc r c) field == Just True then
            drawMatrix
                (i + 1)
                field
                acc ++
                [ rect
                    [ x <| toString <| tileDim * (r)
                    , y <| toString <| tileDim * (c)
                    , width <| toString tileDim
                    , height <| toString tileDim
                    , fill "#11AA11"
                    ]
                    []
                ]
        else if get (loc r c) field == Nothing then
            let
                idx = Debug.log "Incorrect idx: " (loc r c)
            in
                drawMatrix (i + 1) field acc
        else
            drawMatrix (i + 1) field acc


debugInfo : Model -> Html Msg
debugInfo model =
    div []
        [ p [] [ Html.text <| toString model.piece.position ]
        , button [ onClick <| Move Left ] [ Html.text "<" ]
        , button [ onClick <| Rotate ] [ Html.text "^" ]
        , button [ onClick <| Move Right ] [ Html.text ">" ]
        ]

scoreView : Int -> Html Msg
scoreView score =
    p [] [Html.text <| "Score: " ++ toString score]
