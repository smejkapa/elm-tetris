module BlockGenerator exposing (..)

import Random exposing (..)
import BlockType exposing (..)

blockType : Generator BlockType
blockType =
    map fromInt (int 0 6)