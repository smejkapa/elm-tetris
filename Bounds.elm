module Bounds exposing (..)

type alias Bounds =
    { top : Int
    , bot : Int
    , left : Int
    , right : Int
    }