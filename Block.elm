module Block exposing (..)

import BlockType exposing (..)
import Html exposing (..)
import Matrix exposing (..)
import Msg exposing (..)
import Point exposing (..)
import Svg exposing (..)
import Svg.Attributes exposing (..)


blockGridDim : Int
blockGridDim =
    4


tileDim : Int
tileDim =
    25


drawBlock : Block -> List (Html Msg)
drawBlock block =
    drawTile 0 block []


drawTile : Int -> Block -> List (Html Msg) -> List (Html Msg)
drawTile i block tiles =
    let
        lx =
            i % colCount block.grid

        ly =
            i // rowCount block.grid
    in
    if ly >= rowCount block.grid then
        tiles
    else if get (loc lx ly) block.grid == Just True then
        drawTile (i + 1)
            block
            (tiles
                ++ [ rect
                        [ x <| toString <| tileDim * (block.position.x + lx)
                        , y <| toString <| tileDim * (block.position.y + ly)
                        , width <| toString tileDim
                        , height <| toString tileDim
                        , fill "#FFDDDD"
                        ]
                        []
                   ]
            )
    else
        drawTile (i + 1) block tiles


getNextBlock : Point -> Block
getNextBlock point =
    makeBlock T point


makeBlock : BlockType -> Point -> Block
makeBlock t point =
    case t of
        O ->
            Block O
                point
                (Matrix.fromList
                    [ [ False, False, False, False ]
                    , [ False, True, True, False ]
                    , [ False, True, True, False ]
                    , [ False, False, False, False ]
                    ]
                )

        T ->
            Block T
                point
                (Matrix.fromList
                    [ [ False, False, False, False ]
                    , [ False, False, False, True ]
                    , [ False, False, True, True ]
                    , [ False, False, False, True ]
                    ]
                )
        I ->
            Block I
                point
                (Matrix.fromList
                    [ [ False, False, False, False ]
                    , [ True, True, True, True ]
                    , [ False, False, False, False ]
                    , [ False, False, False, False ]
                    ]
                )
        J ->
            Block J
                point
                (Matrix.fromList
                    [ [ False, False, False, False ]
                    , [ False, False, False, True ]
                    , [ False, True, True, True ]
                    , [ False, False, False, False ]
                    ]
                )
        L ->
            Block L
                point
                (Matrix.fromList
                    [ [ False, False, False, False ]
                    , [ False, True, True, True ]
                    , [ False, False, False, True ]
                    , [ False, False, False, False ]
                    ]
                )
        S ->
            Block S
                point
                (Matrix.fromList
                    [ [ False, False, False, True ]
                    , [ False, False, True, True ]
                    , [ False, False, True, False ]
                    , [ False, False, False, False ]
                    ]
                )
        Z ->
            Block Z
                point
                (Matrix.fromList
                    [ [ False, False, False, False ]
                    , [ False, False, True, False ]
                    , [ False, False, True, True ]
                    , [ False, False, False, True ]
                    ]
                )


rotate : Block -> Block
rotate block =
    let
        trans =
            mapWithLocation
                (\l v -> Maybe.withDefault False (get (loc (Matrix.col l) (row l)) block.grid))
                block.grid

        width =
            colCount block.grid

        swap =
            mapWithLocation
                (\l v -> Maybe.withDefault False (get (loc (row l) (width - Matrix.col l - 1)) trans))
                trans
    in
    { block | grid = swap }


transpose : Matrix Bool -> Matrix Bool
transpose mat =
    Matrix.fromList <| transposeJ mat 0 []

transposeJ : Matrix Bool -> Int -> List (List Bool) -> List (List Bool)
transposeJ mat j acc =
    if colCount mat > j then
        transposeJ mat (j + 1) (acc ++ [transposeH mat 0 j []])
    else
        acc


transposeH : Matrix Bool -> Int -> Int -> List Bool -> List Bool
transposeH mat i j acc =
    if rowCount mat > i then
        transposeH mat (i + 1) j (acc ++ [(Maybe.withDefault False (get (loc i j) mat))])
    else
        acc