module Dir exposing (..)

type Dir
    = Left
    | Right
    | Down